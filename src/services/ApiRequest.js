import axios from "axios";

export const baseURL = 'http://localhost:5000/'

export async function sendRequest(url, params, authSession=null) {

    let config = {
        headers: {
        },
    };
    console.log('REQ:', url, params);

    if (authSession) {
        params = {
            username: authSession.username,
            password: authSession.password,
            ...params
        }
    }

    return new Promise((resolve, reject) => {
        axios
            .post(`${baseURL}${url}`, params, config)
            .then((response) => {
                console.log('RES:', url, response.data);
                resolve(response.data)
            })
            .catch((error) => {
                console.error('RES:', url, error.message);
                reject(error.message)
            });
    })
}
