import './css/App.css';
import './css/Form.css';
import './css/Footer.css';
import NavBar from './components/Nav/NavBar';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Page404 from './pages/404';
import Footer from './components/Footer';
import Register from './pages/Register';
import SearchResult from './pages/SearchResult';
import RecepieDetails from './pages/RecepieDetails';
import AddRecepie from './pages/AddRecepie';
import { useEffect } from 'react';
import Bookmarks from './pages/Bookmarks';
import UserRecepies from './pages/UserRecepies';
import Admin from './pages/admin';
import ManageUser from './pages/admin/pages/ManageUser';
import ManageReceipe from './pages/admin/pages/ManageReceipe';

function App({ props }) {

  useEffect(() => {
    console.log('update login status');
  }, [props])

  return (
    <div>
      <HashRouter>
        <NavBar />

        <Switch>
          <Route exact component={() => <Home />} path='/' />
          <Route component={Login} path='/login' />
          <Route component={Register} path='/register' />
          <Route component={SearchResult} path='/search/:searchQuery' />
          <Route component={RecepieDetails} path='/receipe/:id' />
          <Route component={AddRecepie} path='/add-receipe/' />
          <Route component={UserRecepies} path='/user/recepies' />
          <Route component={Bookmarks} path='/user/likes' />
          <Route component={Bookmarks} path='/user/bookmarks' />

          <Route component={Admin} path='/admin' />
          <Route component={Page404} />
        </Switch>

        <br />
        <div className='container'>
          <Footer />
        </div>

      </HashRouter>
    </div>
  );
}

export default App;
