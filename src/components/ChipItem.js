import React from 'react'
import CloseIcon from 'mdi-react/CloseCircleIcon'

export default function ChipItem({
    text,
    color,
    onClick
}) {
    return (
        <div className='chip-item' style={{backgroundColor: color}}  onClick={onClick}>
            {text}
            <CloseIcon size='1.4em' style={{marginLeft: 8}}/>
        </div>
    )
}
