import React, { useState } from 'react'
import StarBorderIcon from 'mdi-react/StarBorderIcon'
import StarHalfFullIcon from 'mdi-react/StarHalfFullIcon'
import StarIcon from 'mdi-react/StarIcon'

export default function StarRatings({
    size,
    fixedValue,
    showValue
}) {
    if (fixedValue) {
        fixedValue = fixedValue > 5 ? 5 : parseFloat(fixedValue).toFixed(1)
        fixedValue = fixedValue < 0 ? 0 : parseFloat(fixedValue).toFixed(1)

        var starData = []
        const base = Math.floor(fixedValue)
        for (let i = 0; i < 5; i++) {
            if (i < base) {
                starData.push('full')
            } else {
                starData.push('none')
            }
        }

        const trailingDigit = fixedValue - base
        if (trailingDigit > 0.4) {
            starData[base] = 'half'
        }

        return <div style={{ display: 'flex', alignItems: 'center' }}>
            {
                starData.map((item, i) => {
                    if (item === 'full')
                        return <StarIcon color='#FFBA00' size={size} key={i} />
                    else if (item === 'half')
                        return <StarHalfFullIcon color='#FFBA00' size={size} key={i} />
                    else
                        return <StarBorderIcon color='#FFBA00' size={size} key={i} />
                })
            }
            {
                showValue && <i style={{ fontSize: 12 }}>({fixedValue})</i>
            }

        </div>
    } else {
        return <div>
        </div>
    }
}



export function StarRatingsInput({
    size,
    onChange,
    defaultValue
}) {
    const [value, setValue] = useState(defaultValue)




    return <div>{
        [1, 1, 1, 1, 1].map((item, i) => {
            if (i < value) {
                return <StarIcon
                    key={i}
                    style={{ cursor: 'pointer' }}
                    size={size}
                    color='#FFBA00'
                    onMouseOver={() => {
                        setValue(i + 1)
                        onChange(i + 1)
                    }}
                />
            } else {
                return <StarBorderIcon
                    key={i}
                    style={{ cursor: 'pointer' }}
                    size={size}
                    color='#FFBA00'
                    onMouseOver={() => {
                        setValue(i + 1)
                        onChange(i + 1)
                    }}
                />
            }
        })
    }
    </div>
}