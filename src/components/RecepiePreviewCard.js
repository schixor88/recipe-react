import React from 'react'
import HeartIcon from 'mdi-react/HeartIcon'
import BookmarkOutlineIcon from 'mdi-react/BookmarkOutlineIcon'
import BookmarkIcon from 'mdi-react/BookmarkIcon'
import '../css/PreviewCard.css'
import { Link } from 'react-router-dom'
import { baseURL, sendRequest } from '../services/ApiRequest'
import { useSelector } from 'react-redux'

export default function RecepiePreviewCard({
    recipeId,
    detailsUrl,
    imageUrl,
    title,
    description,
    author,
    isLiked,
    isBookmarked
}) {
    description = description && description.length > 100 ? description.substr(0, 100) + '...' : description

    const authSession = useSelector(state => state.auth_session)

    return (
        <div className='card preview-card'>
            <Link to={detailsUrl}>
                <img src={`${baseURL}${imageUrl}`} alt={title} />
            </Link>

            <div className='info-container'>
                <Link to={detailsUrl}>
                    <div className='text-container'>
                        <h5>{title}</h5>
                        <p>{description}</p>
                    </div>
                </Link>

                <div className='icon-container'>
                    <HeartIcon className='clickable-icon' size='1.9em' color={isLiked ? '#c50000' : '#000'} />
                    {
                        isBookmarked
                            ? <BookmarkIcon className='clickable-icon' size='2em' color={'#3273d6'} />
                            : <BookmarkOutlineIcon className='clickable-icon' size='2em' />
                    }
                </div>

            </div>

            <div className='author'>@{author}</div>
        </div>
    )
}
