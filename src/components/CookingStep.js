import React from 'react'

export default function CookingStep({
    title,
    description
}) {
    return (
        <div className='cooking-step'>
            <div className='icon' />
            <div className='title'>{title}</div>
            <div className='description'>{description}</div>
        </div>
    )
}
