import React from 'react'
import { Link } from 'react-router-dom'

export default function Footer() {
    return (
        <div className='footer'>
            <hr className='divider' />

            <h1 className='brand-name' style={{ fontSize: 48 }}>
                KUSINA
            </h1>

            <div className='main-container'>
                <div className='content-block'>
                    <div className='title'>Quick Links</div>

                    <Link to='/'>Home</Link>
                    <Link to='/login'>Login</Link>
                    <Link to='/register'>Register</Link>

                </div>

                <div className='content-block'>
                    <div className='title'>Browse By Category</div>
                    <Link to={{pathname: '/search/non-veg', category: 'non-veg'}}>Non-Vegetarian Receipes</Link>
                    <Link to='/search/veg'>Vegetarian Receipes</Link>
                    <Link to='/search/vegan'>Vegan Receipes</Link>
                </div>

                <div className='content-block'>
                    <div className='title'>Contact Us</div>
                    <span>9876543210</span>
                    <span>info@kusina.com</span>
                </div>
            </div>

        </div>
    )
}
