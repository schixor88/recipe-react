import React, { useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import SearchBar from './SearchBar'
import { useSelector } from 'react-redux'
import { set_auth_session } from '../../store/actions'
import { useDispatch } from 'react-redux'

export default function NavBar(props) {

    const authSession = useSelector(state => state.auth_session)

    const history = useHistory()
    const dispatch = useDispatch()

    useEffect(() => {
        console.log('update login status');
    }, [authSession])

    const isLoggedIn = () => {
        return authSession && authSession.username && authSession.password
    }

    const logout = () => {
        dispatch(set_auth_session(null))
        history.push('/')
    }

    return (
        <div className='navbar container'>
            <Link to='/'>
                <div className='brand-name'>KUSINA</div>
            </Link>

            <SearchBar />

            {
                isLoggedIn()
                    ? (
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <Link className='secondary-button' to='/user/recepies'>My Recepies</Link>
                            <Link className='secondary-button' style={{ marginLeft: 24 }} to='/user/bookmarks'>Bookmarks</Link>
                            <Link className='secondary-button' style={{ marginLeft: 24 }} to='/user/likes'>Likes</Link>
                            <Link className='primary-button-outlined' style={{ marginLeft: 24, fontWeight: 'bold' }} to='/add-receipe'>Add Recepie</Link>

                            {
                                <Link className='primary-button-outlined' style={{ marginLeft: 24, fontWeight: 'bold' }} to='/admin'>
                                    Admin
                                </Link>
                            }
                            <div className='nav-profile dropdown' style={{ marginLeft: 24 }}>
                                <div className='dropdown-content'>
                                    <i className='username'>@{authSession.username}</i>
                                    <div className='primary-button-outlined logout' onClick={logout}>
                                        Logout
                                    </div>
                                </div>
                            </div>

                        </div>
                    ) : (
                        <div>
                            <Link className='primary-button-outlined' to='/login' style={{ marginLeft: 24, fontWeight: 'bold' }} >Login</Link>
                            <Link className='primary-button-outlined' style={{ marginLeft: 24, fontWeight: 'bold' }} to='/register'>Register</Link>
                        </div>
                    )
            }


        </div>
    )
}

