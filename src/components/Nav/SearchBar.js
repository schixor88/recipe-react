import React, { useState } from 'react'
import SearchIcon from 'mdi-react/SearchIcon'
import { useHistory } from 'react-router'
import { useSelector } from 'react-redux'

export default function SearchBar() {

    const [searchQuery, setSearchQuery] = useState('')
    const authSession = useSelector(state => state.auth_session)


    const history = useHistory()
    

    const openSearchPage = (e) => {
        e.preventDefault()
        if (!authSession) {
            alert('Please login first')
            return
        }

        if (searchQuery.trim()) {
            history.push(`/search/${searchQuery.trim()}`)
        } else {
            // match all
            history.push(`/search/ `)
        }
    }

    return (
        <form className='searchbar' onSubmit={openSearchPage}>
            <input value={searchQuery} placeholder='Search...' onChange={e => setSearchQuery(e.target.value)} />
            <SearchIcon className='icon' onClick={openSearchPage} />
        </form>
    )
}
