import React, { useEffect, useState } from 'react'
import '../css/Filter.css'
import AddCircleIcon from 'mdi-react/AddCircleIcon'
import ChipItem from './ChipItem'

export default function Filter({
    keywords,
    onUpdateFilter
}) {

    useEffect(() => {
        setKeywordsInput(keywords)
    }, [keywords])

    const [keywordsInput, setKeywordsInput] = useState(keywords||'')
    const [includeInput, setIncludeInput] = useState('')
    const [excludeInput, setExcludeInput] = useState('')

    const [includedIngredients, setIncludedIngredients] = useState([])
    const [excludedIngredients, setExcludedIngredients] = useState([])

    const handleIncludeAdd = () => {
        if (!includeInput) return
        let items = includedIngredients
        items.push(includeInput)
        setIncludedIngredients(items)
        setIncludeInput('')
    }

    const handleExcludeAdd = () => {
        if (!excludeInput) return
        let items = excludedIngredients
        items.push(excludeInput)
        setExcludedIngredients(items)
        setExcludeInput('')
    }

    const removeInclude = (text) => {
        setIncludedIngredients(includedIngredients.filter(item => item !== text))
    }

    const removeExclude = (text) => {
        setExcludedIngredients(excludedIngredients.filter(item => item !== text))
    }

    const handleSubmit = () => {
        onUpdateFilter(keywordsInput, includedIngredients, excludedIngredients)
    }

    return (
        <div className='filter-wrapper container'>

            <div className='filter-options'>
                <div className='title'>Filters</div>

                <div className='filter-option'>
                    <div className='input-container input-sm'>
                        <label>Keywords</label>
                        <input value={keywordsInput} onChange={e => setKeywordsInput(e.target.value)} />
                    </div>
                </div>

                <div className='filter-option'>
                    <div className='input-container input-sm'>
                        <label>Include these Ingredients</label>
                        <input value={includeInput} onChange={e => setIncludeInput(e.target.value)} />
                        <AddCircleIcon className='add-button' color='#444' onClick={handleIncludeAdd} />
                    </div>
                    <div className='chip-container'>
                        {
                            includedIngredients.map((item, i) => {
                                return <ChipItem
                                    color='#008307'
                                    text={item}
                                    key={i}
                                    onClick={() => removeInclude(item)}
                                />
                            })
                        }
                    </div>

                </div>

                <div className='filter-option'>
                    <div className='input-container input-sm'>
                        <label>Exclude these Ingredients</label>
                        <input value={excludeInput} onChange={e => setExcludeInput(e.target.value)} />
                        <AddCircleIcon className='add-button' color='#444' onClick={handleExcludeAdd} />
                    </div>
                    <div className='chip-container'>
                        {
                            excludedIngredients.map((item, i) => {
                                return <ChipItem
                                    color='#960000'
                                    text={item}
                                    key={i}
                                    onClick={() => removeExclude(item)}
                                />
                            })
                        }
                    </div>
                </div>

                <div className='filter-option'>
                    <button className='primary-button submit-button' onClick={handleSubmit}>Update Results</button>
                </div>

            </div>
        </div>
    )
}

