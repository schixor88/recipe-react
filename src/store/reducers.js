import { combineReducers } from "redux";
import { SET_AUTH_SESSION, SET_USER_DETAILS } from "./actions";

function auth_session(state = null, action) {
    switch (action.type) {
        case SET_AUTH_SESSION:
            return state = action.payload;
        default:
            return state;
    }
};


function user_details(state = null, action) {
    switch (action.type) {
        case SET_USER_DETAILS:
            return state = action.payload;
        default:
            return state;
    }
};

export default combineReducers({
    auth_session: auth_session,
    user_details: user_details
})