export const SET_AUTH_SESSION = 'SET_AUTH_SESSION';
export const SET_USER_DETAILS = 'SET_USER_DETAILS';

export const set_auth_session = (auth_session) => {
    return {
        type: SET_AUTH_SESSION,
        payload: auth_session
    }
}

export const set_user_details = (user_details) => {
    return {
        type: SET_USER_DETAILS,
        payload: user_details
    }
}
