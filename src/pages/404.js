import React, { useEffect } from 'react'

export default function Page404() {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <div className='page'>
            404
        </div>
    )
}
