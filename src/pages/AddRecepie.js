import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import '../css/AddRecepie.css'
import '../css/RecepieDetails.css'
import { baseURL, sendRequest } from '../services/ApiRequest'

export default function AddRecepie() {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const authSession = useSelector(state => state.auth_session)

    const [category, setCategory] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')

    const [ingredients, setIngredients] = useState([])
    const [newIngredientName, setNewIngredientName] = useState('')
    const [newIngredientQuantity, setNewIngredientQuantity] = useState('')
    const [newIngredientUnit, setNewIngredientUnit] = useState('')

    const [steps, setSteps] = useState([])
    const [newStep, setNewStep] = useState('')

    const [selectedImagePreview, setSelectedImagePreview] = useState(null)
    const [selectedImage, setSelectedImage] = useState(null)


    const addNewIngredient = () => {
        if (newIngredientName === '' || newIngredientQuantity === '') {
            return
        }

        var temp = ingredients
        temp.push({
            name: newIngredientName,
            quantity: newIngredientQuantity,
            unit: newIngredientUnit
        })

        setIngredients(temp)
        setNewIngredientName('')
        setNewIngredientQuantity('')
        setNewIngredientUnit('')
    }

    const addNewStep = () => {
        if (newStep === '') {
            return
        }

        var temp = steps
        temp.push(newStep)

        setSteps(temp)
        setNewStep('')
    }

    const handleImageSelect = (event) => {
        if (event.target.files && event.target.files[0]) {
            let reader = new FileReader()
            reader.onload = (e) => {
                setSelectedImagePreview(e.target.result)
            };
            reader.readAsDataURL(event.target.files[0]);
            setSelectedImage(event.target.files[0])
        }
    }

    const uploadReceipe = () => {

        if (!selectedImage) {
            alert('Image is required')
            return
        }

        var data = new FormData();
        data.append('username', authSession.username);
        data.append('name', title);
        data.append('description', description);
        data.append('category', category);
        data.append('ingredients', JSON.stringify(ingredients));
        data.append('steps', JSON.stringify(steps.map((item, i) => {
            return { sn: i + 1, todo: item }
        })));
        data.append('productImage', selectedImage);

        axios.post(`${baseURL}recipe/AddRecipe`, data, {
            headers: {
                'Content-Type': 'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2),
            },
            onUploadProgress: (e) => {
                let progress = parseInt(Math.round((e.loaded * 100) / e.total));
                console.log('Upload progress', progress);
            },
        }).then((res) => {
            console.log(res);
            if (res.data.success) {
                alert(res.data.message)

                // clear form
                setCategory('')
                setTitle('')
                setDescription('')
                setIngredients([])
                setNewIngredientName('')
                setNewIngredientQuantity('')
                setNewIngredientUnit('')
                setSteps([])
                setNewStep('')
                setSelectedImage(null)
                setSelectedImagePreview(null)
            } else {
                alert(res.data.message)
            }
        }).catch((error) => {
            alert(error.message)
        });
    }

    return (
        <div className='page container add-recepie '>
            <div className='card'>

                <div className='large-title'>Add Recepie</div>
                <div className='input-container'>
                    <label>Category</label>
                    <select value={category} onChange={(e) => setCategory(e.target.value)} >
                        <option value=''>Select Category</option>
                        <option value='veg'>Vegetarian</option>
                        <option value='non-veg'>Non-Vegetarian</option>
                        <option value='vegan'>Vegan</option>
                    </select>
                </div>

                <div className='input-container'>
                    <label>Receipe Title</label>
                    <input value={title} onChange={(e) => setTitle(e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Description</label>
                    <textarea value={description} onChange={(e) => setDescription(e.target.value)} />
                </div>

                <br />
                <b>Ingredients</b>
                <ul>
                    {
                        ingredients.map((item, i) => {
                            return <li key={i}>
                                {item.name} - {item.quantity} {item.unit}
                            </li>
                        })
                    }
                </ul>
                <div className='input-container' style={{ display: 'flex', flexDirection: 'row' }}>
                    <input placeholder='Ingredient Name' style={{ width: 200 }} value={newIngredientName} onChange={(e) => setNewIngredientName(e.target.value)} />
                    <input placeholder='Quantity' style={{ marginLeft: 12, width: 70 }} value={newIngredientQuantity} onChange={(e) => setNewIngredientQuantity(e.target.value)} />
                    <select style={{ marginLeft: 12 }} value={newIngredientUnit} onChange={(e) => setNewIngredientUnit(e.target.value)} >
                        <option value=''>Select Unit</option>
                        <option value='tsp'>Tablespoon</option>
                        <option value='tbsp'>Teaspoon</option>
                        <option value='cup'>Cup</option>
                        <option value='g'>Grams</option>
                        <option value='kg'>Kilograms</option>
                        <option value='lbs'>Pounds</option>
                        <option value='mL'>Millilitre</option>
                        <option value='l'>Litre</option>
                    </select>
                    <button className='primary-button' style={{ marginLeft: 12, minWidth: 100 }} onClick={addNewIngredient}>Add</button>
                </div>

                <br />
                <b>Steps</b>
                <ol>
                    {
                        steps.map((item, i) => {
                            return <li key={i}>
                                {item}
                            </li>
                        })
                    }
                </ol>
                <div className='input-container'>
                    <label>Step Details</label>
                    <textarea placeholder='Setps info' style={{ minHeight: 80 }} value={newStep} onChange={(e) => setNewStep(e.target.value)} />
                </div>
                <button className='primary-button' style={{ width: 100, float: 'right', marginTop: 10 }} onClick={addNewStep} >Add Step</button>
                <div style={{ clear: 'both' }}></div>

                <div className='input-container'>
                    <label> Select Image</label>
                    <input type='file' id='image-select' onChange={handleImageSelect} />
                    <div className='file-image-preview'>
                        <img src={selectedImagePreview} alt='' />
                    </div>
                </div>

                <button className='primary-button' style={{ width: '100%', marginTop: 24 }} onClick={uploadReceipe}>Upload Receipe</button>
            </div>

            <div className='preview-container'>
                {/* TODO */}
                {/* Preview */}
            </div>
        </div>
    )
}
