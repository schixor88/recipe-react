import React, { useEffect } from 'react'
import '../css/Home.css'

export default function Home() {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    return (
        <div className='page home'>
            <div className='images-container'>
                <div className='left'>
                    <img src={'/images/pasta.jpeg'} alt='' />
                </div>
                <div className='mid'>
                    <img src={'/images/chicken.jpeg'} alt=''/>
                </div>
                <div className='right'>
                    <img src={'/images/chatamari.jpeg'} alt=''/>
                </div>
            </div>
        </div>
    )
}
