import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import Filter from '../components/Filter';
import RecepiePreviewCard from '../components/RecepiePreviewCard'
import { sendRequest } from '../services/ApiRequest';
import { set_user_details } from '../store/actions';

export default function UserRecepies(props) {

    const dispatch = useDispatch()

    const authSession = useSelector(state => state.auth_session)
    const userDetails = useSelector(state => state.user_details)

    const [receipeList, setReceipeList] = useState([])
    const [userBookmarks, setUserBookmarks] = useState([])

    useEffect(() => {
        window.scrollTo(0, 0)
        getUserDetails()
        getRecepieList()
    }, [])

    useEffect(() => {
        setUserBookmarks(
            userDetails.bookmarks?.map(item => item.recipeid)
        )
    }, [userDetails])

    const getUserDetails = () => {
        // TODO:
        let params = {
            username: authSession.username,
            password: authSession.password
        }
        sendRequest('auth/Login', params).then(res => {
            if (res.success) {
                dispatch(set_user_details(
                    res.data
                ))
            }
        }).catch(err => {
        })
    }

    const getRecepieList = () => {
        sendRequest('recipe/AllRecipe', {}, authSession).then(res => {
            if (res.success) {
                setReceipeList(res.data)
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div className='page'>
            <div className='list-view container'>
                {
                    receipeList.map((item, i) => {
                        if (item.createdBy === userDetails.username)
                            return <RecepiePreviewCard
                                key={i}
                                recipeId={item._id}
                                imageUrl={item.productImage}
                                title={item.name}
                                description={item.description}
                                detailsUrl={`/receipe/${item._id}`}
                                author={item.createdBy}
                                isLiked={false}
                                isBookmarked={userBookmarks?.includes(item._id)}
                            />
                    })
                }
            </div>
        </div>
    )
}
