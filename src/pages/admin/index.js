import React from 'react'
import { Link, Route, Switch } from 'react-router-dom'
import ManageReceipe from './pages/ManageReceipe'
import ManageUser from './pages/ManageUser'

export default function Admin() {

    return (
        <div className='page container' style={{ marginTop: 20 }}>
            <div style={{ display: 'flex' }}>
                <Link style={{ color: '#fff', height: '100%' }} to='/admin/manage-users'>
                    <button className='primary-button'>
                        Manage Users
                </button>
                </Link>
                <Link style={{ color: '#fff' }} to='/admin/manage-receipes'  style={{ marginLeft: 10 }}>
                    <button className='primary-button'>
                        Manage Recepies
                </button>
                </Link>
            </div>
            <div>
                <Switch>
                    <Route exact path='/admin' component={() => <h1>Admin</h1>} />
                    <Route path='/admin/manage-users' component={ManageUser} />
                    <Route path='/admin/manage-receipes' component={ManageReceipe} />
                </Switch>
            </div>
        </div>
    )
}
