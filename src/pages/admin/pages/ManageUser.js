import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { sendRequest } from '../../../services/ApiRequest'

export default function ManageUser() {

    const authSession = useSelector(state => state.auth_session)
    const [userList, setUserList] = useState([])

    useEffect(() => {
        getUserList()
    }, [])


    const getUserList = () => {
        sendRequest('auth/AllUsers', {}, authSession).then(res => {
            if (res.success) {
                setUserList(res.data)
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    const setUserStatus = (username, newStatus) => {
        let params = {
            activateFor: username,
            activate: newStatus
        }
        sendRequest('auth/ActivateUser', params, authSession).then(res => {
            if (res.success) {
                getUserList()
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div>
            <h1>Manage Users</h1>
            <table className='admin-table'>
                <thead >
                    <tr>
                        <th style={{ width: '10%' }}>SN</th>
                        <th style={{ width: '20%' }}>Full Name</th>
                        <th style={{ width: '20%' }}>Username</th>
                        <th style={{ width: '30%' }}>Email</th>
                        <th style={{ width: '10%' }}>Status</th>
                        <th style={{ width: '20%' }}>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        userList.map((item, i) => {
                            return <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.fullName}</td>
                                <td>{item.username}</td>
                                <td>{item.email}</td>
                                {
                                    item.isActive
                                        ? <td style={{backgroundColor: '#0db10d44', textAlign: 'center'}}>Active</td>
                                        : <td style={{backgroundColor: '#96000044', textAlign: 'center'}}>Inactive</td>
                                }
                                
                                <td>{item.isActive
                                    ? <button className='primary-button-sm' onClick={()=>setUserStatus(item.username, false)}>Deactivate</button>
                                    : <button className='primary-button-sm' onClick={()=>setUserStatus(item.username, true)}>Activate</button>
                                }
                                </td>
                            </tr>
                        })
                    }

                </tbody>
            </table>
        </div>
    )
}
