import React, { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import StarRatings from '../../../components/StarRatings'
import { sendRequest } from '../../../services/ApiRequest'
import EyeIcon from 'mdi-react/EyeIcon'
import EditIcon from 'mdi-react/EditIcon'
import { useHistory } from 'react-router'

export default function ManageReceipe() {

    const authSession = useSelector(state => state.auth_session)
    const [receipeList, setReceipeList] = useState([])

    const history = useHistory()

    useEffect(() => {
        getAllReceipes()
    }, [])

    const getAllReceipes = () => {
        sendRequest('recipe/AllRecipe', {}, authSession).then(res => {
            if (res.success) {
                setReceipeList(res.data)
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div>
            <h1>Manage Receipes</h1>
            <table className='admin-table'>
                <thead >
                    <tr>
                        <th style={{ width: '10%' }}>SN</th>
                        <th style={{ width: '25%' }}>Title</th>
                        <th style={{ width: '40%' }}>Description</th>
                        <th style={{ width: '10%' }}>Ratings</th>
                        <th style={{ width: '15%' }}>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        receipeList.map((item, i) => {
                            return <tr key={i}>
                                <td>{i + 1}</td>
                                <td>{item.name}</td>
                                <td>{item.description}</td>
                                <td><StarRatings fixedValue={4} showValue /> </td>
                                <td>
                                    <EyeIcon
                                        style={{ backgroundColor: '#076b92', padding: 4, borderRadius: 4, cursor: 'pointer' }}
                                        color='#fff'
                                        onClick={()=>history.push(`/receipe/${item._id}`)}
                                    />
                                    <EditIcon
                                        style={{ backgroundColor: '#079225', padding: 4, borderRadius: 4, marginLeft: 4, cursor: 'pointer' }}
                                        color='#fff'
                                    />
                                </td>
                            </tr>
                        })
                    }

                </tbody>
            </table>
        </div>
    )
}
