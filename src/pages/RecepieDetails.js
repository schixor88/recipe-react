import BookmarkOutlineIcon from 'mdi-react/BookmarkOutlineIcon'
import BookmarkIcon from 'mdi-react/BookmarkIcon'
import HeartIcon from 'mdi-react/HeartIcon'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CookingStep from '../components/CookingStep'
import StarRatings, { StarRatingsInput } from '../components/StarRatings'
import '../css/RecepieDetails.css'
import { baseURL, sendRequest } from '../services/ApiRequest'
import { set_user_details } from '../store/actions'

export default function RecepieDetails({
    match
}) {

    const dispatch = useDispatch()

    const [recipeId, setReceipeId] = useState(match.params.id)
    const [userBookmarks, setUserBookmarks] = useState([])
    const [receipeDetails, setReceipeDetails] = useState({})
    const [isBookmarked, setIsBookmarked] = useState(false)
    const [isLiked, setIsLiked] = useState(false)

    const [ratingStars, setRatingStars] = useState(5)
    const [reviewInput, setReviewInput] = useState('')
    const [avgRating, setAvgRating] = useState(5)


    const authSession = useSelector(state => state.auth_session)
    const userDetails = useSelector(state => state.user_details)

    const [conversionType, setConversionType] = useState('default')


    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    useEffect(() => {
        setUserBookmarks(
            userDetails.bookmarks?.map(item => item.recipeid)
        )
    }, [userDetails])

    useEffect(() => {
        setIsBookmarked(userBookmarks?.includes(recipeId))
    }, [userBookmarks])

    useEffect(() => {
        setReceipeId(match.params.id)
    }, [match])

    useEffect(() => {
        getUserDetails()
        getReceipeDetails()
    }, [recipeId])

    const getUserDetails = () => {
        // TODO: Use different api
        let params = {
            username: authSession.username,
            password: authSession.password
        }
        sendRequest('auth/Login', params).then(res => {
            if (res.success) {
                dispatch(set_user_details(
                    res.data
                ))
            }
        }).catch(err => {
        })
    }

    const getReceipeDetails = () => {

        let params = {
            recipeId: recipeId
        }

        sendRequest('recipe/GetRecipeById', params, authSession).then(res => {
            if (res.success) {
                setReceipeDetails(res.data)

                var totalRating = 0
                var totalUsers = 0

                res.data.comments?.forEach(item => {
                    totalRating += item.rating ?? 5
                    totalUsers += 1
                });

                if (totalUsers > 0) {
                    setAvgRating(totalRating / totalUsers)
                }
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    const addToBookmarks = () => {
        let params = {
            recipeName: receipeDetails.name,
            recipeId: receipeDetails._id
        }
        sendRequest('recipe/BookmarkRecipe', params, authSession).then(res => {
            if (res.success) {
                setIsBookmarked(true)
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    const removeFromBookmarks = () => {
        let params = {
            recipeName: receipeDetails.name,
            recipeId: receipeDetails._id
        }
        sendRequest('recipe/RemoveBookmark', params, authSession).then(res => {
            if (res.success) {
                setIsBookmarked(false)
            } else {
                alert(res.message)
            }

        }).catch(err => {
            alert(err)
        })
    }

    const submitReview = () => {
        let params = {
            recipeId: receipeDetails._id,
            comment: reviewInput,
            rating: ratingStars
        }
        sendRequest('recipe/CommentRecipe', params, authSession).then(res => {
            if (res.success) {
                getReceipeDetails()
                alert('Review Submitted')
            } else {
                alert(res.message)
            }

        }).catch(err => {
            alert(err)
        })
    }

    const toggleLike = () => {
        setIsLiked(!isLiked)
    }

    return (
        <div className='page container recepie-details'>
            <div className='left-container'>
                <div>
                    <div className='title-container'>
                        <div className='large-title'>{receipeDetails.name}</div>

                        <div style={{ width: 100 }}>
                            <HeartIcon className='clickable-icon' size='1.9em' color={isLiked ? '#c50000' : '#000'} onClick={toggleLike} />
                            {
                                isBookmarked
                                    ? <BookmarkIcon className='clickable-icon' style={{ marginLeft: 10 }} size='2em' onClick={removeFromBookmarks} color={'#3273d6'} />
                                    : <BookmarkOutlineIcon className='clickable-icon' style={{ marginLeft: 10 }} size='2em' onClick={addToBookmarks} />
                            }
                        </div>
                    </div>

                    <div className='username'>@{receipeDetails.createdBy}</div>
                    <StarRatings fixedValue={avgRating} showValue />
                </div>

                <p>{receipeDetails.description}</p>

                <hr />
                <div className='ingredients'>

                    <div className='small-title'>Ingredients</div> <span>
                        <select onChange={e=>setConversionType(e.target.value)}>
                            <option value='default'>Conversion Default</option>
                            <option value='metric'>Metric</option>
                            <option value='imperial'>Imperial</option>
                        </select>
                    </span>
                    <ul>
                        {
                            receipeDetails.ingredients && receipeDetails.ingredients?.map((item, i) => {
                                return <li key={i}>{`${item.name} ${convertValue(conversionType, item.quantity, item.unit)}`}</li>
                            })
                        }
                    </ul>
                </div>

                <hr />
                <div>
                    <div className='small-title'>Directions</div>
                    {
                        receipeDetails.steps && receipeDetails.steps?.map((item, i) => {
                            return <CookingStep
                                key={i}
                                title={`Step ${item.sn}`}
                                description={item.todo}
                            />
                        })
                    }
                </div>


                <hr />

                <div>
                    <div className='small-title'>Reviews</div>

                    {
                        receipeDetails.comments?.map((item, i) => {
                            return <div className='review-card'>
                                <div className='username'>@{item.username}</div>
                                <StarRatings size='1.5em' fixedValue={item.rating ?? 5} />
                                <div>
                                    {item.comment}
                                </div>
                            </div>
                        })
                    }

                    <hr />
                    <div className='small-title'>Add Rating &amp; Review</div>

                    <br />
                    <StarRatingsInput size={'2.5em'} defaultValue={ratingStars} onChange={value => {
                        setRatingStars(value)
                    }} />

                    <div className='input-container'>
                        <textarea placeholder='Write your review...' value={reviewInput} onChange={e => setReviewInput(e.target.value)} />
                    </div>
                    <br />
                    <button className='primary-button' style={{ float: 'right', width: '10em' }} onClick={submitReview}>Submit</button>
                </div>

            </div>

            <div className='right-container'>
                <img className='card' src={`${baseURL}${receipeDetails.productImage}`} alt='' />

            </div>
        </div>
    )
}

function convertValue(type, quantity, unit) {
    var number;
    try {
        number = parseFloat(quantity)
    } catch (error) {
        return `${quantity} ${unit ?? ''}`
    }

    if (type == 'default' || unit == '') {
        return `${quantity} ${unit ?? ''}`
    } else if (type == 'metric') {
        switch (unit) {
            case 'tsp': return `${(quantity*4.92892).toFixed(1)} mL`
            case 'tbsp': return `${(quantity*14.7868).toFixed(1)} mL`
            case 'cup': return `${(quantity*236.588).toFixed(1)} mL`
            case 'lbs': return `${(quantity*453.592).toFixed(1)} g`
        
            default:
                return `${quantity} ${unit ?? ''}`
        }

    } else if (type == 'imperial') {
        switch (unit) {
            case 'g': return `${(quantity*0.035274).toFixed(1)} oz`
            case 'kg': return `${(quantity*2.20462).toFixed(1)} lbs`
            case 'mL': return `${(quantity*0.067628).toFixed(1)} tbsp`
            case 'l': return `${(quantity*4.22675).toFixed(1)} cups`
        
            default:
                return `${quantity} ${unit ?? ''}`
        }
    }
}