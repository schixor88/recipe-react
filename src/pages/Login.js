import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { sendRequest } from '../services/ApiRequest';
import { set_auth_session, set_user_details } from '../store/actions';

export default function Login() {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const [formData, setFormData] = useState({
        username: '',
        password: '',
    })

    const dispatch = useDispatch()
    const history = useHistory()

    const updateFormData = (name, newValue) => {
        setFormData({
            ...formData,
            [name]: newValue,
        });
    };

    const submitLogin = () => {
        let params = {
            username: formData.username,
            password: formData.password
        }
        sendRequest('auth/Login', params).then(res => {
            if (res.success) {
                alert(res.message)
                dispatch(set_auth_session({
                    username: formData.username,
                    password: formData.password
                }))

                dispatch(set_user_details(
                    res.data
                ))

                if (res.data.isAdmin) {  
                    history.push('/admin')
                } else {
                    history.push('/')
                }
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div className='page'>
            <div className='login-card card'>
                <div className='title'>Login</div>

                <br />

                <div className='input-container'>
                    <label>Username</label>
                    <input value={formData.username} onChange={(e) => updateFormData('username', e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Password</label>
                    <input type='password' value={formData.password} onChange={(e) => updateFormData('password', e.target.value)} />
                </div>

                <button className='primary-button' style={{ marginTop: 32 }} onClick={submitLogin}>Login</button>
            </div>
        </div>
    )
}
