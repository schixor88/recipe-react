import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { sendRequest } from '../services/ApiRequest';

export default function Register() {

    useEffect(() => {
        window.scrollTo(0, 0)
    }, [])

    const history = useHistory()

    const [formData, setFormData] = useState({
        name: '',
        username: '',
        email: '',
        password: '',
        confirmPassword: '',
    })

    const updateFormData = (name, newValue) => {
        setFormData({
            ...formData,
            [name]: newValue,
        });
    };

    const registerUser = () => {
        if (formData.password !== formData.confirmPassword) {
            alert('Password and Confirm Password do not match')
            return
        }

        let params = {
            fullName: formData.name,
            username: formData.username,
            email: formData.email,
            password: formData.password
        }

        sendRequest('auth/RegisterUser', params).then(res => {
            if(res.success) {
                alert(res.message)
                history.push('/login')
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div className='page'>
            <div className='login-card card'>
                <div className='title'>Create a New Account</div>

                <br />

                <div className='input-container'>
                    <label>Full Name</label>
                    <input value={formData.name} onChange={(e) => updateFormData('name', e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Username</label>
                    <input value={formData.username} onChange={(e) => updateFormData('username', e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Email</label>
                    <input value={formData.email} onChange={(e) => updateFormData('email', e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Password</label>
                    <input type='password' value={formData.password} onChange={(e) => updateFormData('password', e.target.value)} />
                </div>

                <div className='input-container'>
                    <label>Confirm Password</label>
                    <input type='password' value={formData.confirmPassword} onChange={(e) => updateFormData('confirmPassword', e.target.value)} />
                </div>

                <button className='primary-button' style={{ marginTop: 32 }} onClick={registerUser}>Sign Me Up</button>
            </div>
        </div>
    )
}
