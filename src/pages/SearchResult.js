import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import Filter from '../components/Filter';
import RecepiePreviewCard from '../components/RecepiePreviewCard'
import { sendRequest } from '../services/ApiRequest';
import { set_user_details } from '../store/actions';

export default function SearchResult(props) {

    const dispatch = useDispatch()

    const [searchQuery, setSearchQuery] = useState(props.match.params.searchQuery)

    const authSession = useSelector(state => state.auth_session)
    const userDetails = useSelector(state => state.user_details)

    const [receipeList, setReceipeList] = useState([])
    const [userBookmarks, setUserBookmarks] = useState([])


    useEffect(() => {
        window.scrollTo(0, 0)
        setSearchQuery(props.match.params.searchQuery)

    }, [props])

    useEffect(() => {
        getRecepieList(searchQuery, null, null)
        getUserDetails()
    }, [searchQuery])

    useEffect(() => {
        setUserBookmarks(
            userDetails.bookmarks?.map(item => item.recipeid)
        )
    }, [userDetails])

    const updateFilterQueries = (keywords, included, excluded) => {
        getRecepieList(keywords, included, excluded)
    }

    const getUserDetails = () => {
        // TODO: Use different api
        // no user details api available, login again to get details
        let params = {
            username: authSession.username,
            password: authSession.password
        }
        sendRequest('auth/Login', params).then(res => {
            if (res.success) {
                dispatch(set_user_details(
                    res.data
                ))
            }
        }).catch(err => {
        })
    }

    const getRecepieList = (keywords, included, excluded) => {
        console.log('FILTER', keywords, included, excluded);
        sendRequest('recipe/AllRecipe', {}, authSession).then(res => {
            if (res.success) {
                // keyword filter
                var filteredList = res.data
                if (keywords) {
                    const keywordArr = keywords.split(' ')

                    const category = keywordArr.includes('veg') ? 'veg'
                        : keywordArr.includes('non-veg') ? 'non-veg'
                            : keywordArr.includes('vegan') ? 'vegan'
                                : null

                    filteredList = filteredList.filter(recepieItem => {
                        var containsKeyword = false

                        keywordArr.forEach(keyword => {
                            if (recepieItem.name.toLowerCase().includes(keyword.toLowerCase()))
                                containsKeyword = true

                            if (recepieItem.category.toLowerCase().includes(keyword.toLowerCase()))
                                containsKeyword = true

                        });

                        if (category) {
                            console.log('Contains cagegory', category);
                            containsKeyword = recepieItem.category == category
                        }
                        return containsKeyword
                    })
                }

                // include ingredient
                if (included && included.length > 0) {
                    filteredList = filteredList.filter(recepieItem => {
                        var allIngredientsString = recepieItem.ingredients
                            .reduce((acc, item) => { return acc + ' ' + item.name }, '').toLowerCase()

                        var containsIngredient = false
                        included.forEach(ingredient => {
                            containsIngredient = allIngredientsString.includes(ingredient.toLowerCase())
                        });
                        return containsIngredient
                    })
                }

                // exclude ingredient
                if (excluded && excluded.length > 0) {
                    filteredList = filteredList.filter(recepieItem => {
                        var allIngredientsString = recepieItem.ingredients
                            .reduce((acc, item) => { return acc + ' ' + item.name }, '').toLowerCase()

                        var containsIngredient = false
                        excluded.forEach(ingredient => {
                            containsIngredient = allIngredientsString.includes(ingredient.toLowerCase())
                        });
                        return !containsIngredient
                    })
                }


                setReceipeList(filteredList)
            } else {
                alert(res.message)
            }
        }).catch(err => {
            alert(err)
        })
    }

    return (
        <div className='page'>
            {
                !props.disableFilter && (
                    <Filter
                        keywords={searchQuery}
                        onUpdateFilter={updateFilterQueries}
                    />
                )
            }

            <div className='list-view container'>
                {
                    receipeList.map((item, i) => {
                        return <RecepiePreviewCard
                            key={i}
                            recipeId={item._id}
                            imageUrl={item.productImage}
                            title={item.name}
                            description={item.description}
                            detailsUrl={`/receipe/${item._id}`}
                            author={item.createdBy}
                            isLiked={false}
                            isBookmarked={userBookmarks?.includes(item._id)}
                        />
                    })
                }
            </div>
        </div>
    )
}
